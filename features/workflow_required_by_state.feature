@workflow-required-by-state @api
Feature: Field required flag depends on workflow states.
  In order to ensure that some fields get filled out during specific workflow states
  As an Site Builder
  I need a field's required flag to depend on workflow states.

  Background:
    Given I am logged in as a "Administrator"

  @javascript
  Scenario Outline: Fields' required flag depends on workflow states during entity (node) creation.
    Given I am on "<NODE_ADD_PATH>"
     Then the checkbox "First" should be checked
      And the "Dependent field" field is required
      And the "Another dependent field" field is not required
     When I select the radio button "Second"
     Then the "Dependent field" field is required
      And the "Another dependent field" field is required
     When I select the radio button "Third"
     Then the "Dependent field" field is not required
      And the "Another dependent field" field is required
     When I select the radio button "First"
     Then the "Dependent field" field is required
      And the "Another dependent field" field is not required
    Examples:
      | NODE_ADD_PATH                         |
      | /node/add/test_content                |
      | /node/add/test_content_ief            |
      | /node/add/test_content_multilevel_ief |

  @javascript
  Scenario Outline: Fields' required flag depends on workflow states when editing exiting entities (nodes).
    Given I am on "<NODE_ADD_PATH>"
      And I fill in "Test node" for "Title"
      And I fill in "Test" for "Dependent field"
      And I select "red" from "Select list dependent field"
      And I press "Save"
     When I click "Edit" in the "tabs" region
     Then the checkbox "First" should be checked
      And the "Dependent field" field is required
      And the "Another dependent field" field is not required
      And the "Select list dependent field" field is not required
     When I select the radio button "Second"
      And I fill in "Test" for "Another dependent field"
      And I press "Save"
     When I click "Edit" in the "tabs" region
      And the "Dependent field" field is required
      And the "Another dependent field" field is required
      And the "Select list dependent field" field is required
     When I select the radio button "Third"
      And I press "Save"
     When I click "Edit" in the "tabs" region
     Then the "Dependent field" field is not required
      And the "Another dependent field" field is required
      And the "Select list dependent field" field is not required
    Examples:
      | NODE_ADD_PATH              |
      | /node/add/test_content     |
      | /node/add/test_content_ief |

  @javascript
  Scenario Outline: Fields' required flag depends on workflow states when editing exiting entities (nodes with multi-level IEF forms).
    Given I am on "/node/add/test_content_multilevel_ief"
      And I fill in "Test node" for "Title"
      And I fill in "Test" for "Dependent intermediate field"
      And I fill in "Test" for "Dependent field"
      And I select "red" from "Select list dependent field"
      And I press "Save"
     When I click "Edit" in the "tabs" region
     Then the checkbox "First" should be checked
      And the "Dependent field" field is required
      And the "Another dependent field" field is not required
      And the "Select list dependent field" field is not required
     When I select the radio button "Second"
      And I fill in "Test" for "Another dependent field"
      And I press "Save"
     When I click "Edit" in the "tabs" region
      And the "Dependent field" field is required
      And the "Another dependent field" field is required
      And the "Select list dependent field" field is required
     When I select the radio button "Third"
      And I press "Save"
     When I click "Edit" in the "tabs" region
     Then the "Dependent field" field is not required
      And the "Another dependent field" field is required
      And the "Select list dependent field" field is not required

  @javascript
  Scenario: Fields' required flag depends on workflow states when editing exiting entities (nodes) with multi-level IEFs.
    Given I am on "/node/add/test_content_multilevel_ief"
      And I fill in "Test node" for "Title"
      And I fill in "Test" for "Dependent intermediate field"
      And I fill in "Test" for "Dependent field"
      And I press "Save"
     When I click "Edit"
     Then the checkbox "First" should be checked
      And the "Dependent intermediate field" field is required
      And the "Dependent field" field is required
      And the "Another dependent field" field is not required
     When I select the radio button "Second"
      And I fill in "Test" for "Another dependent field"
      And I press "Save"
     When I click "Edit"
      And the "Dependent intermediate field" field is required
      And the "Dependent field" field is required
      And the "Another dependent field" field is required
     When I select the radio button "Third"
      And I press "Save"
     When I click "Edit"
     Then the "Dependent intermediate field" field is not required
      And the "Dependent field" field is not required
      And the "Another dependent field" field is required

