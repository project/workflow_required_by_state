@field-settings @api
Feature: Field settings are persisted as third-party settings.
  In order to persist settings related to each field instance
  As an Developer
  I need to write third-party settings into field instance configs.

  Background:
    Given I am logged in as a "Administrator"
      And I run "cd modules/contrib/workflow_required_by_state; git checkout modules/workflow_required_by_state_test/config/install/config_enforce.registry.workflow_required_by_state_test.yml"
      And I run "cd modules/contrib/workflow_required_by_state; git checkout modules/workflow_required_by_state_test/config/install/field.field.node.test_content.field_third_party_settings.yml"
      And I run "drush cee"

  @javascript
  Scenario: Our settings do not appear if a field is set to required.
    Given I am on "admin/structure/types/manage/test_content/fields/node.test_content.field_third_party_settings"
     When I check "Required field"
     Then I should not see "Workflow required settings"
     When I uncheck "Required field"
     Then I should see "Workflow required settings"

  @javascript
  Scenario: Our settings appear on field instance config forms.
     When I am on "admin/structure/types/manage/test_content/fields/node.test_content.field_third_party_settings"
     Then I should see "Workflow required settings"
      And the "Override the required flag per workflow state." checkbox should not be checked
     When I check "Override the required flag per workflow state."
      And I press "Save settings"
     # I guess the formatting of the message is making it not be detected as a single line.
     # So we test for each of the parts of the message here. Stupid, I know...
     Then I should see the following success messages:
          | Success messages     |
          | Saved                |
          | Third party settings |
          | configuration.       |
     When I am on "admin/structure/types/manage/test_content/fields/node.test_content.field_third_party_settings"
     Then the "Override the required flag per workflow state." checkbox should be checked

  @javascript
  Scenario: Our settings appear in enforced config.
    Given I am on "admin/structure/types/manage/test_content/fields/node.test_content.field_third_party_settings"
      And the "Override the required flag per workflow state." checkbox should not be checked
      And I check "Override the required flag per workflow state."
      And I press "Save settings"
     When I run "cd modules/contrib/workflow_required_by_state; git diff modules/workflow_required_by_state_test/config/install/field.field.node.test_content.field_third_party_settings.yml"
     Then I should get:
          """
           +  module:
           +    - workflow_required_by_state
           +third_party_settings:
           +  workflow_required_by_state:
           +    enabled: true
          """

  Scenario: Clean up field changes. (This just runs the Background)
