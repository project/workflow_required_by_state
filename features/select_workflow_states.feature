@select-workflow-states @api
Feature: Select workflow states that will make the field required.
  In order for a field's required flag to depend on workflow states
  As an Site Builder
  I need to select which workflow states will make the field required.

  Background:
    Given I am logged in as a "Administrator"
      And I run "cd modules/contrib/workflow_required_by_state; git checkout modules/workflow_required_by_state_test/config/install/config_enforce.registry.workflow_required_by_state_test.yml"
      And I run "cd modules/contrib/workflow_required_by_state; git checkout modules/workflow_required_by_state_test/config/install/field.field.node.test_content.field_third_party_settings.yml"
      And I run "drush cee"

  @javascript
  Scenario: A field to select workflow states appears on field instance config forms.
    Given I am on "admin/structure/types/manage/test_content/fields/node.test_content.field_third_party_settings"
      And I should not see "Select the workflow states that will make this field required"
     When I check "Override the required flag per workflow state."
      And I select "Test workflow" from "Select the workflow on which this field should depend"
      And I should see "Select the workflow states that will make this field required"
      And the checkbox "First" should be unchecked
      And the checkbox "Second" should be unchecked
      And the checkbox "Third" should be unchecked

  @javascript
  Scenario: We can select workflow states on field instance config forms.
    Given I am on "admin/structure/types/manage/test_content/fields/node.test_content.field_third_party_settings"
     When I check "Override the required flag per workflow state."
      And I select "Test workflow" from "Select the workflow on which this field should depend"
      And I check "First"
      And I press "Save settings"
     Then I should see the following success messages:
          | Success messages     |
          | Saved                |
          | Third party settings |
          | configuration.       |
     When I am on "admin/structure/types/manage/test_content/fields/node.test_content.field_third_party_settings"
     Then the checkbox "First" should be checked

  @javascript
  Scenario: Our settings appear in enforced config.
    Given I am on "admin/structure/types/manage/test_content/fields/node.test_content.field_third_party_settings"
      And I check "Override the required flag per workflow state."
      And I select "Test workflow" from "Select the workflow on which this field should depend"
      And I check "First"
      And I press "Save settings"
     When I run "cd modules/contrib/workflow_required_by_state; git diff modules/workflow_required_by_state_test/config/install/field.field.node.test_content.field_third_party_settings.yml"
     Then I should get:
          """
           +  module:
           +    - workflow_required_by_state
           +third_party_settings:
           +  workflow_required_by_state:
           +    enabled: true
           +    workflow: test_workflow
           +    workflow_states:
           +      - test_workflow_first
          """

  Scenario: Clean up field changes. (This just runs the Background)
