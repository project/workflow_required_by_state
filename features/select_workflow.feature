@select-workflow @api
Feature: Select the workflow on which a field will depend.
  In order to select which workflow states on which a field will depend
  As an Site Builder
  I need to select the workflow from which to select states.

  Background:
    Given I am logged in as a "Administrator"
      And I run "cd modules/contrib/workflow_required_by_state; git checkout modules/workflow_required_by_state_test/config/install/config_enforce.registry.workflow_required_by_state_test.yml"
      And I run "cd modules/contrib/workflow_required_by_state; git checkout modules/workflow_required_by_state_test/config/install/field.field.node.test_content.field_third_party_settings.yml"
      And I run "drush cee"

  @javascript
  Scenario: A field to select workflows appears on field instance config forms.
    Given I am on "admin/structure/types/manage/test_content/fields/node.test_content.field_third_party_settings"
      And I should not see "Select the workflow on which this field should depend"
     When I check "Override the required flag per workflow state."
     Then I should see "Select the workflow on which this field should depend"
      And the "Select the workflow on which this field should depend" field should not contain "test_workflow"

  @javascript
  Scenario: We can select a workflow on field instance config forms.
    Given I am on "admin/structure/types/manage/test_content/fields/node.test_content.field_third_party_settings"
     When I check "Override the required flag per workflow state."
      And I select "Test workflow" from "Select the workflow on which this field should depend"
      And I press "Save settings"
     Then I should see the following success messages:
          | Success messages     |
          | Saved                |
          | Third party settings |
          | configuration.       |
     When I am on "admin/structure/types/manage/test_content/fields/node.test_content.field_third_party_settings"
     Then the "Select the workflow on which this field should depend" field should contain "test_workflow"

  @javascript
  Scenario: Our settings appear in enforced config.
    Given I am on "admin/structure/types/manage/test_content/fields/node.test_content.field_third_party_settings"
      And I check "Override the required flag per workflow state."
      And I select "Test workflow" from "Select the workflow on which this field should depend"
      And I press "Save settings"
     When I run "cd modules/contrib/workflow_required_by_state; git diff modules/workflow_required_by_state_test/config/install/field.field.node.test_content.field_third_party_settings.yml"
     Then I should get:
          """
           +  module:
           +    - workflow_required_by_state
           +third_party_settings:
           +  workflow_required_by_state:
           +    enabled: true
           +    workflow: test_workflow
          """

  Scenario: Clean up field changes. (This just runs the Background)
