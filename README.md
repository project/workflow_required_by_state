## INTRODUCTION

The Workflow Required by State module allows fields to be required only when
the associated entity (or parent entity) is in a specific workflow state.

**N.B.** This module extends the contrib
[Workflow](https://www.drupal.org/project/workflow) module, and not core's
content-moderation workflows.

The primary use case for this module is:

- To make complex multi-participant workflows less error-prone.

## REQUIREMENTS

This module requires the [Workflow](https://www.drupal.org/project/workflow) module.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

You'll need the following before being able to configure this module's settings.
- Add one or more workflows and populate them with some workflow states.
- Add a content type and add a workflow field to it.
- Add a field that you want to make dependent on the workflow state.

To configure this module's settings:
- Open the settings of field that you want to make dependent on the workflow state.
- Ensure that the "Required" checkbox is *not* checked.
- Check the "Override the required flag per workflow state" checkbox.
- Select the desired workflow from the "Select the workflow on which this field should depend" list.
- Check the desired states from the "Select the workflow states that will make this field required" checkboxes.

## MAINTAINERS

Current maintainers for Drupal 10:

- Christopher Gervais (ergonlogic) - https://www.drupal.org/u/ergonlogic

